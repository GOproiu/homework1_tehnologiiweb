// Dat fiind că :
//
//          funcția distance primește ca parametrii două array-uri
//          fiecare element poate apărea cel mult o dată într-un array; orice apariții suplimentare sunt ignorate
//          distanța dintre cele 2 array-uri este numărul de elemente diferite dintre ele
//          dacă parametrii nu sunt array-uri se va arunca o excepție (InvalidType)
//
// Completați următoarele cerințe
//
//          distance returnează rezultatul corect pentru 2 array-uri (20%)
//          distance ignoră elementele duplicate (20%)
//          distance aruncă InvalidType dacă unul dintre parametri nu este un array (20%)
//          distance returnează 0 la compararea a 2 array-uri vide (20%)
//          distance diferențiază între numere și string-uri reprezentând numere (20%)

function eliminareDuplicate(item, index, myArray) { 
    return myArray.indexOf(item) === index;
}

function distance(first, second){
	let result = 0

    if(Array.isArray(first) === false || Array.isArray(second) === false ){
		throw new Error("InvalidType")
	}
	
	//Solution1
	let firstArrayFiltered = first.filter(eliminareDuplicate)
	let secondArrayFiltered = second.filter(eliminareDuplicate)

	//Solution2 - Easier than Solution1
	// let firstArrayFiltered = Array.from(new Set(first))
	// let secondArrayFiltered = Array.from(new Set(second))

    for (let i = 0; i < firstArrayFiltered.length ; i++) {
        if(firstArrayFiltered[i] !== secondArrayFiltered[i]){
			result++
		}
	}

	for (let i = 0; i < secondArrayFiltered.length; i++) {
		if(secondArrayFiltered[i] !== firstArrayFiltered[i]){
			result++
		}
	}
    return result
}

module.exports.distance = distance
